angular.module('myApp', [])
        .service('myService', function ($http) {
            var apiMP3 = 'http://www.youtubeinmp3.com/fetch/?format=json&video=http://www.youtube.com/watch?v=';

            this.getMP3 = function (id) {
                return $http.get(apiMP3 + id);
            };

        })

        .controller('myCtrl', function ($scope, $timeout, myService, $sce, $interval) {
            $scope.items = [];

            $scope.logout = function () {
                document.getElementById('foto').src = 'semfoto.jpg';
                gapi.auth.signOut();
                $scope.items = [];
                $('#inputSearch').attr('disabled', 'disabled');
                $('#buttonSearch').attr('disabled', 'disabled');
                $('#login').css({
                    'display': 'block'
                });
                localStorage.removeItem('userPicture');
                localStorage.removeItem('userName');
            }

            $interval(function () {
                var picture = localStorage.getItem('userPicture');
                var name = localStorage.getItem('userName');
                if (picture && name) {
                    $scope.isLogged = true;
                    $timeout(function () {
                        var div = document.getElementById("nameUser");
                        div.innerHTML = "" + name;
                    });
                } else {
                    $scope.isLogged = false;
                }
            }, 1000);


            $scope.searchKey = function (key) {
                if (key) {
                    var request = gapi.client.youtube.search.list({
                        q: key,
                        part: 'snippet',
                        maxResults: 12
                    });
                    request.execute(function (response) {
                        $timeout(function () {
                            $scope.items = response.result.items;
                        });
                    });
                } else {
                    $scope.items = [];
                    $('#login').css({
                        'display': 'none'
                    })
                }
            }

            $scope.change = function (event, key) {
                if (event.keyCode === 13) {
                    $scope.searchKey(key);
                }
            }

            $scope.getURL = function (id) {
                return $sce.trustAsResourceUrl('http://embed.yt-mp3.com/watch?v=' + id + '&downloadbtn=true');
            }

        });