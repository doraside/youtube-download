

var picture = localStorage.getItem('userPicture');
var name = localStorage.getItem('userName');
if (picture) {
    $('#inputSearch').removeAttr('disabled');
    $('#buttonSearch').removeAttr('disabled');
    document.getElementById('foto').src = picture;
    document.getElementById("nameUser").innerHTML = ""+name;
    $('#login').css({
        'display': 'none'
    });
} else {
    $('#inputSearch').attr('disabled', 'disabled');
    $('#buttonSearch').attr('disabled', 'disabled');
    $('#login').css({
        'display': 'block'
    })
}
// // The client ID is obtained from the Google Developers Console
// at https://console.developers.google.com/.
// If you run this code from a server other than http://localhost,
// you need to register your own client ID.
var OAUTH2_CLIENT_ID = '162514689601-cu51vj4quha2s4n8ilb5bjsmfav9oh88.apps.googleusercontent.com';
var OAUTH2_SCOPES = [
    'https://www.googleapis.com/auth/youtube',
    'https://www.googleapis.com/auth/userinfo.profile',
    'https://www.googleapis.com/auth/plus.me'
];

// Upon loading, the Google APIs JS client automatically invokes this callback.
googleApiClientReady = function () {
    gapi.auth.init(function () {
        window.setTimeout(checkAuth, 1);
    });
}

// Attempt the immediate OAuth 2.0 client flow as soon as the page loads.
// If the currently logged-in Google Account has previously authorized
// the client specified as the OAUTH2_CLIENT_ID, then the authorization
// succeeds with no user intervention. Otherwise, it fails and the
// user interface that prompts for authorization needs to display.
function checkAuth() {
    gapi.auth.authorize({
        client_id: OAUTH2_CLIENT_ID,
        scope: OAUTH2_SCOPES,
        immediate: true
    }, handleAuthResult);
}

// Handle the result of a gapi.auth.authorize() call.
function handleAuthResult(authResult) {
    if (authResult && !authResult.error) {
        // Authorization was successful. Hide authorization prompts and show
        // content that should be visible after authorization succeeds.
        $('.pre-auth').hide();
        $('.post-auth').show();
        loadAPIClientInterfaces();
    } else {
        signIn()
    }
}

function signIn() {
    var request = gapi.auth.authorize({
        client_id: OAUTH2_CLIENT_ID,
        scope: OAUTH2_SCOPES,
        immediate: false
    }, handleAuthResult);



    request.then(function (resp) {
        getUser();
    });
}

function getUser() {
    gapi.client.load('plus', 'v1').then(function () {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        request.then(function (resp) {
            console.log(resp)
            document.getElementById('foto').src = resp.result.image.url;
            $("#nameUser").html(""+resp.result.displayName);
            localStorage.setItem('userPicture', resp.result.image.url);
            localStorage.setItem('userName', resp.result.displayName);            
            $('#inputSearch').removeAttr('disabled');
            $('#buttonSearch').removeAttr('disabled');
            $('#inputSearch').focus();
            $('#login').css({
                'display': 'none'
            });
        }, function (reason) {
            console.log('Error: ' + reason.result.error.message);
        });
    });
}


// Load the client interfaces for the YouTube Analytics and Data APIs, which
// are required to use the Google APIs JS client. More info is available at
// http://code.google.com/p/google-api-javascript-client/wiki/GettingStarted#Loading_the_Client
function loadAPIClientInterfaces() {
    gapi.client.load('youtube', 'v3', function () {
        handleAPILoaded();
    });
}